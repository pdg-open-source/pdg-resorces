import React from 'react'
import ReactDOM from 'react-dom'
import Link from 'gatsby-link'
import Logo from '../Logo/Logo'
import styled from 'styled-components'
import Img from 'gatsby-image'


const HeaderWrapper = styled.div`
  overflow: hidden;
  position: relative;
  background-color: #fee140;
  background-image: linear-gradient(155deg, #fee140 0%, #fa709a 100%);
  margin-bottom: 1.45rem;
  height: ${({ isHome }) => (isHome ? '70vh' : '20vh')};
  h1 {
    img {
      height: 100px;
      margin: 0;
    }
    margin: 0;
  }
`

const HeaderContainer = styled.div`
  margin: 0 auto;
  max-width: 960px;
  padding: 1.45rem 1.0875rem;
  display: flex;
  align-items: center;
  justify-content: between;
  position: relative;
  z-index: 2;
  position: flex;
`

const Navigation = styled.nav`
  ul {
    list-style: none;
    display: flex;
    justify-content: between;
    li {
      margin-left: 10px
      font-family: Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans,
      Helvetica Neue, sans-serif;
      text-transform: uppercase;
      > a {
        color: #ffffff;
        text-decoration: none;
        &:hover{
          color: rgba(0, 0, 0, 0.8);
        }
      }
    }
  }
`
class Header extends React.Component {

  render() {
    const { data, location } = this.props
    return <HeaderWrapper>
            <HeaderContainer>
              <h1>
                <Link to="/">
                  <Logo></Logo>
                </Link>
              </h1>
              <Navigation>
                <ul>
                  <li>
                    <Link to="/">home</Link>
                  </li>
                  <li>
                    <Link to="/about">About</Link>
                  </li>
                </ul>
              </Navigation>
            </HeaderContainer>
            <Img style={{ position: 'absolute', left: 0, top: 0, width: `100%`, height: `100%`, opacity: 0.3 }} sizes={data.background.sizes} />
        </HeaderWrapper>
  }
}

export default Header
