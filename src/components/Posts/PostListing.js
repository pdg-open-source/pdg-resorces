import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

const Date = styled.div`
  font-style: italic;
  font-size: 1rem;
  font-weight:100;
  margin-top: 15px;
`

const PostListing = ({ post }) => (
  <article>
    <h2>
      <Link to={post.fields.slug}>{post.frontmatter.title}</Link>
      <Date>
        {post.frontmatter.date}
      </Date>
    </h2>
    <p>{post.excerpt}</p>
  </article>
)

export default PostListing;